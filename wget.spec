Name:          wget
Version:       1.25.0
Release:       1
Summary:       A package for retrieving files using HTTP, HTTPS, FTP and FTPS the most widely-used Internet protocols.
License:       GPL-3.0-or-later AND LGPL-2.1-or-later
Url:           https://www.gnu.org/software/wget/
Source:        https://ftp.gnu.org/gnu/wget/wget-%{version}.tar.gz

Patch0:        wget-1.25.0-etc-path.patch

Provides:      webclient bundled(gnulib)
BuildRequires: make perl-HTTP-Daemon python3 libuuid-devel perl-podlators libpsl-devel libmetalink-devel
BuildRequires: gnutls-devel pkgconfig texinfo gettext autoconf libidn2-devel gpgme-devel zlib-devel
BuildRequires: gcc

%description
GNU Wget is a free software package for retrieving files using HTTP, HTTPS,
FTP and FTPS the most widely-used Internet protocols. It is a non-interactive
commandline tool, so it may easily be called from scripts, cron jobs, terminals
without X-Windows support, etc.

%package_help

%prep
%autosetup -p1

%build
%configure --with-ssl=gnutls --with-libpsl --enable-largefile --enable-opie --enable-digest --enable-ntlm --enable-nls --enable-ipv6 --disable-rpath --with-metalink --disable-year2038

%make_build

%install
%make_install CFLAGS="$RPM_OPT_FLAGS"
%find_lang %{name}
%find_lang %{name}-gnulib
rm -f %{buildroot}%{_infodir}/dir

%check
make check

%files -f %{name}.lang -f %{name}-gnulib.lang
%license COPYING
%doc AUTHORS
%config(noreplace) %{_sysconfdir}/wgetrc
%{_bindir}/wget

%files help
%doc MAILING-LIST NEWS README doc/sample.wgetrc
%{_mandir}/man1/wget.*
%{_infodir}/*

%changelog
* Thu Jan 23 2025 Funda Wang <fundawang@yeah.net> - 1.25.0-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update to 1.25.0

* Thu Nov 21 2024 Han Jinpeng <hanjinpeng@kylinos.cn> - 1.21.4-3
- Type:CVE
- ID:CVE-2024-10524
- SUG:NA
- DESC: fix CVE-2024-10524 and also fix Print message issue

* Sun Jun 16 2024 xuchenchen <xuchenchen@kylinos.cn> - 1.21.4-2
- Type:CVES
- ID:NA
- SUG:NA
- DESC:backport CVE-2024-38428

* Fri Jul 28 2023 xingwei <xingwei14@h-partners.com> - 1.21.4-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update wget to 1.21.4

* Fri Feb 03 2023 xingwei <xingwei14@h-partners.com> - 1.21.3-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update wget to 1.21.3

* Sat Oct 22 2022 gaihuiying <eaglegai@163.com> - 1.21.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix find_cell(): wget killed by SIGSEGV

* Tue Mar 22 2022 xihaochen <xihaochen@huawei.com> - 1.21.2-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update wget to 1.21.2

* Fri Jul 30 2021 gaihuiying <gaihuiying1@huawei.com> - 1.20.3-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build error with gcc10

* Thu May 27 2021 lijingyuan <lijingyuan3@huawei.com> - 1.20.3-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add the compilation dependency of gcc.

* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 1.20.3-3
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update source url

* Thu Apr 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.20.3-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Sanitize input param dl_total_time

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.20.3-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:Package upgrade

* Sat Sep 14 2019 huzhiyu<huzhiyu1@huawei.com> - 1.19.5-6
- Package init
